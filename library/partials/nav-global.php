
<!-- Header -->
<header id="header" class="clearfix"><meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<div id="header-wrap">
<div class="row">
<div class="columns medium-12">

<!-- Primary Navigation -->
<div id="primary-menu-trigger"><i class="fa fa-bars"></i></div>

<!-- Logo -->
<div id="logo">
<a href="<?php echo $home_url; ?>" class="standard-logo"><img src="<?php  bloginfo('template_url');  ?>/assets/images/logo-small.png" alt="Thorum Wealth Managment"></a>
</div>
<!-- #logo end -->

<nav id="primary-menu">
<ul class="menu single-page-nav" data-easing="easeInOutExpo" data-speed="1000" data-offset="25">
<li>
<a href="/" data-href="#wrapper">
<div>Home</div>
</a>
</li>
<li>
<a href="/ffblog">
<div>Blog</div>
</a>
</li>
<li>
<a href="/commentary">
<div>Commentary</div>
</a>
</li>
<li>
<a href="#" data-href="/about-section">
<div>About</div>
</a>
</li>
<li>
<a href="#" data-href="/services">
<div>Services</div>
</a>
</li>
<li>
<a href="#" data-href="/#contact-section">
<div>Contact</div>
</a>
</li>
<li>
<a href="https://myaccountviewonline.com/" data-href="#client-section">
<div>Client Login</div>
</a>
</li>
</ul>
</nav>
<!-- END #primary-menu -->
</div>
</div>
</div>
</header>
<!-- END #header -->
