
<!-- CTA Life Mangement -->
<div class="row expanded common-height">
<div class="medium-6 columns column-padding" style="background: url(<?php  bloginfo('template_url');  ?>/assets/images/page/service-bg.jpg) center center no-repeat; background-size: cover;">
</div>
<div class="medium-6 columns column-padding" style="background-color: #F8F8F8;">
<div>
<p class="lead nobottommargin">While the numbers alone won't offer you happiness, understanding your numbers will help to guide you on your path.</p>
<div class="row text-center">
<div class="columns large-6">
<div class="counter-item-block bottom-margin-sm">
<div class="icon-large">
<i class="lnr lnr-layers"></i>
</div>
<div class="counter counter-lined">
<span data-from="100" data-to="846" data-refresh-interval="50" data-speed="2000"></span>
</div>
<h5>Pizza's eaten</h5>
</div>
</div>
<div class="columns large-6">
<div class="counter-item-block bottom-margin-sm">
<div class="icon-large">
<i class="lnr lnr-camera-video"></i>
</div>
<div class="counter counter-lined">
<span data-from="3000" data-to="12400" data-refresh-interval="100" data-speed="2500"></span>
</div>
<h5>Movie hours</h5>
</div>
</div>
</div>
<div class="row text-center">
<div class="columns large-6">
<div class="counter-item-block bottom-margin-sm">
<div class="icon-large">
<i class="lnr lnr-location"></i>
</div>
<div class="counter counter-lined">
<span data-from="10" data-to="427" data-refresh-interval="25" data-speed="3500"></span>
</div>
<h5>Days spent travelling</h5>
</div>
</div>
<div class="columns large-6">
<div class="counter-item-block bottom-margin-sm">
<div class="icon-large">
<i class="lnr lnr-coffee-cup"></i>
</div>
<div class="counter counter-lined">
<span data-from="60" data-to="1260" data-refresh-interval="30" data-speed="2700"></span>
</div>
<h5>Coffee cups consumed</h5>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END CTA Life Management -->
