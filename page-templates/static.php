<?php
/*
Template Name: Static Page Template
*/
get_header();

// Inital Template Partials
get_template_part( 'library/partials/nav', 'global' ); 
get_template_part( 'library/partials/splash', 'small' ); 
?>

<div id="base">

<!-- Main Content -->
<div id="main-content" class="row">

<section id="page" role="main">

<?php do_action( 'cms_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('content') ?> id="post-<?php the_ID(); ?>">
      <header>
          <h1 class="entry-title"><?php the_title(); ?></h1>
      </header>
      <?php do_action( 'cms_page_before_entry_content' ); ?>
      <div class="entry-content">
          <?php the_content(); ?>
      </div>
      <footer>
          <?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'cms' ), 'after' => '</p></nav>' ) ); ?>
          <p><?php the_tags(); ?></p>
      </footer>
      
  </article>
<?php endwhile;?>

<?php do_action( 'cms_after_content' ); ?>

</section>
<!-- End Page Section -->

</div>
<!-- End Main Content -->

</div>
<!-- ./base -->

<?php
get_template_part( 'library/partials/section', 'contact' );
get_footer(); 
?>