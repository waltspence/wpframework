
<!-- Featured -->
<div class="section-page row">
<div class="columns medium-12">
<div class="section-title-block text-center centered-div" style="max-width: 800px;">
<h2 class="title text-uppercase">Featured In</h2>
<p class="lead">Thorium has been featured in some of these major publications</p>
</div>
<div id="oc-clients" class="owl-carousel image-carousel">
<div class="oc-item"><a target="_blank" href="http://www.wsj.com/articles/when-a-wealthy-client-has-little-in-liquid-assets-1413562057"><img src="<?php  bloginfo('template_url');  ?>/assets/images/nnews/wsj.png" alt="Wall Street Journal – October 17, 2014"></a></div>
<div class="oc-item"><a target="_blank" href="http://www.fiduciarynews.com/2014/11/retirement-saving-how-responsible-is-the-401k-fiduciary/"><img src="<?php  bloginfo('template_url');  ?>/assets/images/nnews/fn.png" alt="Fiduciary News –  Nov. 4, 2014"></a></div>
<div class="oc-item"><a target="_blank" href="http://www.investmentnews.com/article/20150202/FREE/150209994/why-advisers-are-worried-about-emoneys-acquisition-by-fidelity"><img src="<?php  bloginfo('template_url');  ?>/assets/images/nnews/in.png" alt="Investment News  - February 2, 2015"></a></div>
<div class="oc-item"><a target="_blank" href="http://www.cnbc.com/2015/02/25/top-reasons-for-early-retirement-account-withdrawals.html"><img src="<?php  bloginfo('template_url');  ?>/assets/images/nnews/cnbc.png" alt="CNBC –  February 25, 2015"></a></div>
<div class="oc-item"><a target="_blank" href="http://finance.yahoo.com/news/comes-emergency-fund-050000185.html"><img src="<?php  bloginfo('template_url');  ?>/assets/images/nnews/yf.png" alt="Yahoo Finance –  February 22, 2016"></a></div>
<div class="oc-item"><a target="_blank" href="http://www.forbes.com/sites/jrose/2016/02/28/13-financial-advisors-share-the-worst-financial-mistakes-they-have-ever-seen/#2fed94155743"><img src="<?php  bloginfo('template_url');  ?>/assets/images/nnews/forbes.png" alt="Forbes – February 28, 2016"></a></div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {

var ocClients = $("#oc-clients");

ocClients.owlCarousel({
margin: 60,
loop: true,
nav: false,
autoplay: true,
dots: false,
autoplayHoverPause: true,
responsive:{
0:{ items:2 },
480:{ items:3 },
768:{ items:4 },
992:{ items:5 },
1200:{ items:6 }
}
});

});

</script>
</div>
</div>
<!-- END Featured -->
