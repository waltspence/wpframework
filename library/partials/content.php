<?php
/**
 * The default template for displaying content
 *
 *@package ThoriumWP
 *@since ThoriumWP 1.0.0
*
*/

?>

<section class="entry">
<article id="post-<?php the_ID(); ?>" <?php post_class('post-entry'); ?>>
	<header class="post">
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php cms_entry_meta(); ?>
	</header>
	<div class="entry-content">
		<?php if(!$post->post_excerpt) {
			the_content();
			} else {
			the_excerpt();
		} ?>
	</div>
	<footer class="post-tags">
		<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
	</footer>
	<hr class="article">
</article>
</section>