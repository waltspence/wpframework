<?php
/**
 * Change the class for sticky posts to .wp-sticky to avoid conflicts with Foundation's Sticky plugin
 *
 *@package ThoriumWP
 *@since ThoriumWP 1.0.0
 */

if ( ! function_exists( 'cms_sticky_posts' ) ) :
function cms_sticky_posts( $classes ) {
	if ( in_array( 'sticky', $classes, true ) ) {
	    $classes = array_diff($classes, array('sticky'));
	    $classes[] = 'wp-sticky';
	}
	return $classes;
}
add_filter('post_class','cms_sticky_posts');

endif;
