<?php
/**
 * Sidebar Template
 *
 * @package ThoriumWP
 * @since ThoriumWP 1.0.0
 *
 */ 
 ?>
 
 <!-- Nav Sidebar -->
     <!-- This is source ordered to be pulled to the left on larger screens -->
     <div class="Sidebar medium-3 columns">
       	<?php do_action( 'cms_before_sidebar' ); ?>
       	<?php dynamic_sidebar( 'sidebar-widgets' ); ?>
       	<?php do_action( 'cms_after_sidebar' ); ?>
        <br>
       <?php dynamic_sidebar('sidebar-widgets-lower'); ?>
     </div>