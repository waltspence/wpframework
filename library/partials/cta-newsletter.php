
<!-- CTA Newsletter -->
<div class="row">
<div class="columns large-8 bottom-margin-lg">
<div class="heading-block top-margin-lg">
<h3>The Best of Thorium &amp; the web in your inbox.</h3>
</div>
<p>Sign up for our newsletter to get the latest information from Thorium Wealth Management along with articles from around the web that we find useful, interesting, and entertaining.</p>
<a href="http://news.thoriumwealth.com/" class="button">Check It Out</a>
</div>
<div class="columns large-4 text-center top-margin-lg">
<img data-sr="wait 0.2s, enter bottom" src="<?php  bloginfo('template_url');  ?>/assets/images/iphone.png" alt="Iphone">
</div>
</div>
<!-- END CTA Newsletter -->
