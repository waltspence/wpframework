<?php
/**
 * Add Custom Post Statuses to WP
 *
 * @package ThoriumWP
 * @since ThoriumWP 1.0.0
 */

// Register Custom Status
function Compliance_Review() {

	$args = array(
		'label'                     => _x( 'Compliance Review', 'Status General Name', 'text_domain' ),
		'label_count'               => _n_noop( 'Compliance Review (%s)',  'Compliance Reviews (%s)', 'text_domain' ), 
		'public'                    => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'exclude_from_search'       => true,
	);
	register_post_status( 'Compliance_Review', $args );

}
add_action( 'init', 'Compliance_Review', 0 );