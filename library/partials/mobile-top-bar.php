<?php
/**
 * Template part for off canvas menu
 *
 * @package ThoriumWP
 * @since ThoriumWP 1.0.0
 */

?>

<nav class="vertical menu" id="mobile-menu" role="navigation">
  <?php cms_mobile_nav(); ?>
</nav>
