<!-- CTA Riskalyze -->
<div class="section-page dark-bg" style="background-image: url(<?php  bloginfo('template_url');  ?>/assets/images/page/main1.jpg);" data-stellar-background-ratio="0.3">
<div class="row text-center">
<div class="stand-out-title">
<h2>The <strong>best things</strong> in life are <strong>free!</strong>
</h2>
<p class="lead topmargin-sm">Get a <strong>free</strong> portfolio analysis!</p>
</div>
<a href="https://pro.riskalyze.com/embed/370abfa7575dc43c0d76" target="_blank" class="button button-border button-rounded button-light large">Yes Please!</a>
</div>
</div>
<!-- END CTA Riskalayze -->