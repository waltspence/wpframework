<?php
/**
 * Add Custom Post Types to WP
 *
 * @package ThoriumWP
 * @since ThoriumWP 1.0.0
 */
 
// Register Custom Post Types

// Commentary Post Type

function commentary() {

	$labels = array(
		'name'                  => _x( 'Commentaries', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Commentary', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Commentary', 'text_domain' ),
		'name_admin_bar'        => __( 'Commentary', 'text_domain' ),
		'archives'              => __( 'Commentary Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Commentaries', 'text_domain' ),
		'add_new_item'          => __( 'Add Commentary', 'text_domain' ),
		'add_new'               => __( 'Add New Commentary', 'text_domain' ),
		'new_item'              => __( 'New Commentary', 'text_domain' ),
		'edit_item'             => __( 'Edit Commentary', 'text_domain' ),
		'update_item'           => __( 'Update Commentary', 'text_domain' ),
		'view_item'             => __( 'View Commentary', 'text_domain' ),
		'search_items'          => __( 'Search Commentary', 'text_domain' ),
		'not_found'             => __( 'Commentary Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Commentary Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Commentary', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Commentary', 'text_domain' ),
		'items_list'            => __( 'Commentaries list', 'text_domain' ),
		'items_list_navigation' => __( 'Commentaries list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Commentaries list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Commentary', 'text_domain' ),
		'description'           => __( 'Market Commentary', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'page-attributes', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
    'menu_position'       => 7,
    'menu_icon'           => 'dashicons-chart-pie',
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'commentary', $args );

}
add_action( 'init', 'commentary', 0 );


// Podcast Post Type

function podcast_pt() {

  $labels = array(
    'name'                => _x( 'Podcasts', 'Podcast General Name', 'text_domain' ),
    'singular_name'       => _x( 'Podcast', 'Podcast Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Podcasts', 'text_domain' ),
    'parent_item_colon'   => __( 'Parent Podcast:', 'text_domain' ),
    'all_items'           => __( 'All Podcasts', 'text_domain' ),
    'view_item'           => __( 'View Podcast', 'text_domain' ),
    'add_new_item'        => __( 'Add New Podcast', 'text_domain' ),
    'add_new'             => __( 'Add New', 'text_domain' ),
    'edit_item'           => __( 'Edit Podcast', 'text_domain' ),
    'update_item'         => __( 'Update Podcast', 'text_domain' ),
    'search_items'        => __( 'Search Podcasts', 'text_domain' ),
    'not_found'           => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
  );
  $args = array(
    'label'               => __( 'podcasts', 'text_domain' ),
    'description'         => __( 'Podcast Description', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 8,
    'menu_icon'           => 'dashicons-format-audio',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'page'
  );
  register_post_type( 'podcasts', $args );
}

add_action( 'init', 'podcast_pt', 0 );

// Blog Post Type

function FFblog_post() {

	$labels = array(
		'name'                  => _x( 'Finance Factory Blog Posts', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'ffblog', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Finance Factory', 'text_domain' ),
		'name_admin_bar'        => __( 'Finance Factory Posts', 'text_domain' ),
		'archives'              => __( 'Finance Factory Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Blog Posts', 'text_domain' ),
		'add_new_item'          => __( 'Add New Blog Post', 'text_domain' ),
		'add_new'               => __( 'Add New Blog Post', 'text_domain' ),
		'new_item'              => __( 'New Blog Post', 'text_domain' ),
		'edit_item'             => __( 'Edit Blog Post', 'text_domain' ),
		'update_item'           => __( 'Update Blog Post', 'text_domain' ),
		'view_item'             => __( 'View Blog Post', 'text_domain' ),
		'search_items'          => __( 'Search Blog Posts', 'text_domain' ),
		'not_found'             => __( 'Blog Posts Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Blog Posts Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Blog Post', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Blog Post', 'text_domain' ),
		'items_list'            => __( 'Blog Posts list', 'text_domain' ),
		'items_list_navigation' => __( 'Blog Posts list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter Blog Posts list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'ffblog', 'text_domain' ),
		'description'           => __( 'Thoughts from The Finance Factory Posts', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'revisions', 'page-attributes', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
    'menu_icon'             => 'dashicons-format-aside',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'ffblog', $args );

}
add_action( 'init', 'FFblog_post', 0 );