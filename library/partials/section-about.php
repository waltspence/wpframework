
<!-- About -->
<div id="about-section" class="section-page">
<div class="row">
<div class="text-center centered-div" style="max-width: 800px;">
<h1>Welcome to Thorium Wealth Management</h1>
<p class="lead">
We combine our years of experience, 
data-driven decision making, and fanatical hands on approach to help meet your goals.
</p>
</div>
</div>
<div id="team" class="row top-margin bottom-margin">
<div class="large-6 columns">
<div class="team-member-block" data-sr="wait 0.2s, enter left">
<!-- image -->
<div class="team-member-image">
<a href="about/Peter/Bio">
<img src="<?php  bloginfo('template_url');  ?>/assets/images/team/1.jpg" alt="team">
</a>
<div class="team-member-overlay">
</div>
</div>
<!-- caption -->
<div class="team-member-caption">
<h3><a href="about/team/peter">Peter Huminski</a></h3>
<span>CEO, Founder</span>
<div class="team-member-social-links float-right">
<a href="http://twitter.com/thoriumwealth"><i class="fa fa-twitter"></i></a>
<a href="https://www.linkedin.com/pub/peter-huminski-awma%C2%AE/32/554/8ba"><i class="fa fa-linkedin"></i></a>
<a href="http://brokercheck.finra.org/Individual/Summary/4591190"><i class="fa fa-bold"></i></a>
<a href="mailto:Peter.Huminski@ThoriumWealth.com?subject=Inquiry%20from%20ThroiumWealth.com"><i class="fa fa-envelope"></i></a>
</div>
</div>
</div>
</div>
<div class="large-6 columns">
<h3>Meet the Founder Peter Huminski</h3>

<div id="bio-wrap">
<article class="bio">
<p>With over 13 years of experience in wealth management, I am dedicated to helping 
business owners and successful individuals address their financial needs by developing a 
holistic plan focused around their long term goals. My extensive experience – throughout major shifts in the markets – 
enables me to help my clients structure their financial and intellectual capital to 
address their specific financial goals.</p>

<div style="display:none;" id="full-bio">
<p>I began my wealth management career as a private banker with BB&amp;T’s Wealth Management Group where I worked for years. 
In 2005 I joined SunTrust Private Wealth Management as a client advisor. 
In 2012 I moved my practice to Wells Fargo Advisors in Greensboro, NC. 
After evaluating the industry I made the determination that my clients would be best served in a private practice setting.
This led me to make the decision to form Thorium Wealth Management an independent investment and financial services firm.</p>

<p>I have earned my BS from Gettysburg College in Pennsylvania. In addition to applicable securities registrations, 
I hold the Accredited Wealth Management Advisor (AWMA) professional designation. 
I am an active member of the Financial Planning Association (FPA) and the Investment Management Consultants Association (IMCA).</p>

<p>Committed to helping serve my community I have been involved with the Chamber of Commerce, Leadership Winston Salem, 
Next Step Ministries, Korners Folly Foundation, Upward Basketball and 
the Kernersville Rotary Club. In 2006 I was selected as one of the 40 leaders under 40 by The Triad Business Journal. 
(This award was based on criteria including career achievement, leadership and community service involvement of young professionals in the Triad).</p>

<p>My wife Becky and I live in Kernersville with our children Kayla, Savanna, and Andrew. Away from the office I enjoy running, golf, coaching my son’s basketball teams, spending time with my 
family, and following University of Virginia sports. I look forward to learning more about your needs and assisting you in working toward your goals.</p>
</article>
</div>

<a class="button button-border button-toggle button-light large">More</a>
<br>
<span>Check the background of this investment professional on FINRA's <a href="http://brokercheck.finra.org/Individual/4591190">BrokerCheck <i class="fa fa-search"></i></a></span>
<script>
$( ".button" ).click(function() {
$( "#full-bio" ).show( "slow" );
$(".button-toggle").hide();
});
</script>
</div>
<!-- END Bio-wrap -->
</div>
</div>
</div>
<!-- END About -->
