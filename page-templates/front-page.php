<?php
/*
Template Name: Front Page
*/

get_header(); ?>

<?php get_template_part( 'library/partials/nav', 'header' ); 
get_template_part( 'library/partials/splash', 'home' ); ?>

<!-- Main Content -->
<div id="main-content">

<?php 
/** Load main-content partials **/
get_template_part( 'library/partials/section', 'about' );
get_template_part( 'library/partials/cta', 'riskalyze' );
get_template_part( 'library/partials/section', 'services' );
get_template_part( 'library/partials/cta', 'quote' );
get_template_part( 'library/partials/cta', 'newsletter' );
get_template_part( 'library/partials/cta', 'lifemangement' );
get_template_part( 'library/partials/section', 'featured' );
get_template_part( 'library/partials/section', 'contact' );
/** END main-content partials **/
?>

</div>
<!-- End Main Content -->

<?php get_footer(); ?>