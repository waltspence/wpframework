
<!-- Splash -->
<section id="slider" class="slider-parallax full-screen force-full-screen dark-bg">
<div class="full-screen force-full-screen dark section-page no-padding no-margin no-border ohidden" style="background-image: url(<?php  bloginfo('template_url');  ?>/assets/images/page/home_slider.jpg); background-size: cover; background-position: center center; height: 667px">
<div class="row text-center">
<div class="absolute-centered">
<div class="stand-out-title">
<h1>
<span class="text-rotator nocolor" data-separator="|" data-rotate-animation="fadeIn" data-speed="6000">
<span class="t-rotate fw700">Thorium Wealth Management|Data-Driven.|Personal Service.</span>
</span>
</h1>
</div>
<a href="#" class="button hollow light-btn" data-scrollto="#services-section" data-easing="easeInOutExpo" data-speed="1250" data-offset="70">What We Do</a>
</div>
<!-- END .absolute-centered -->
</div>
<div class="video-wrap">
<div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
</div>
<a href="#" data-scrollto="#section-about" data-easing="easeInOutExpo" data-speed="1250" data-offset="65" class="one-page-arrow dark"><i class="icon-angle-down infinite animated fadeInDown"></i></a>
</div>
<!-- END .full-screen -->
</section>
<!-- END splash -->
