<?php
/*
Theme Name:         ThoriumWP
Theme URI:          N/A
Github Theme URI:   N/A
Description:        Proprietary theme for Thorium Wealth Management
Version:            4.0
Author:             Walt Spence of @EnLabHQ | http://enlab.co
Author URI:         http://walt.im/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, etc.

*@link https://codex.wordpress.org/Theme_Development
*@package ThoriumWP
*@since ThoriumWP 1.0.0

*/

// Load CMS

// Various clean up functions
require_once( 'library/functions/cleanup.php' );

// Required for Foundation to work properly
require_once( 'library/functions/foundation.php' );

// Register all navigation menus
require_once( 'library/functions/navigation.php' );

// Add custom post types
require_once( 'library/functions/post-types.php' );

// Add custom post statuses
require_once( 'library/functions/post-statuses.php' );

// Create widget areas in sidebar and footer
require_once( 'library/functions/widget-areas.php' );

// Return entry meta information for posts
require_once( 'library/functions/entry-meta.php' );

//Enqueue scripts
require_once( 'library/functions/enqueue-scripts.php' );

// Add theme support
require_once( 'library/functions/theme-support.php' );

// Add Nav Options to Customer
require_once( 'library/functions/custom-nav.php' );

// Change WP's sticky post class
require_once( 'library/functions/sticky-posts.php' );

// Configure responsive image sizes
require_once( 'library/functions/responsive-images.php' );

// Sharify Social Buttons
require_once( 'library/functions/sharify.php' );

/******/

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );r

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );

//End Functions
 ?>