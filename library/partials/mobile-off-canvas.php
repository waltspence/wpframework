<?php
/**
 * Template part for off canvas menu
 *
 * @package ThoriumWP
 * @since ThoriumWP 1.0.0
 */

?>

<nav class="off-canvas position-left" id="mobile-menu" data-off-canvas data-position="left" role="navigation">
  <?php cms_mobile_nav(); ?>
</nav>

<div class="off-canvas-content" data-off-canvas-content>
