<?php
/**
 * Register widget areas
 *
 *@package ThoriumWP
 *@since ThoriumWP 1.0.0
 */

if ( ! function_exists( 'cms_sidebar_widgets' ) ) :
function cms_sidebar_widgets() {
	register_sidebar(array(
	  'id' => 'sidebar-widgets',
	  'name' => __( 'Sidebar widgets', 'cms' ),
	  'description' => __( 'Drag widgets to this sidebar container.', 'cms' ),
	  'before_widget' => '<div id="%1$s" class="widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));
	
	register_sidebar(array(
		'id' => 'sidebar-widgets-lower',
		'name' => __( 'Sidebar widgets', 'cms' ),
		'description' => __( 'Drag widgets to this sidebar container.', 'cms' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));

	register_sidebar(array(
	  'id' => 'footer-widgets',
	  'name' => __( 'Footer widgets', 'cms' ),
	  'description' => __( 'Drag widgets to this footer container', 'cms' ),
	  'before_widget' => '<div id="%1$s" class="large-4 columns widget %2$s">',
	  'after_widget' => '</div>',
	  'before_title' => '<h6>',
	  'after_title' => '</h6>',
	));
}

add_action( 'widgets_init', 'cms_sidebar_widgets' );
endif;
