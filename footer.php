<!-- Footer -->
<!-- Footer Content -->
<footer id="footer" class="dark-bg">
<div class="row">
<div class="text-center wp-icon-animate wp-icon-effect-tfb">
<i class="wp-icon wp-icon-small wp-icon-twitter fa fa-twitter"><a href="https://twitter.com/ThoriumWealth"></a></i>
<i class="wp-icon wp-icon-small wp-icon-facebook fa fa-facebook"><a href="https://facebook.com/ThoriumWealthManagement/"></a></i>
<i class="wp-icon wp-icon-small wp-icon-bold fa fa-bold"><a href="http://brokercheck.finra.org/Individual/Summary/4591190"></a></i>
<i class="wp-icon wp-icon-small wp-icon-envelope fa fa-envelope"><a href="mailto:Peter.Huminski@ThoriumWealth.com?subject=Inquiry from ThroiumWealth.com"></a></i>
</div> 
<div class="legal text-center">
<p>© 2014-16 Thorium Wealth Management, LLC. All Rights Reserved. | Webdesign by F5 Studios</p>
<p>Securities offered through LPL Financial, Member <a href="http://finra.org">FINRA</a> / <a href="http://sipc.org">SIPC</a>. Investment advice offered through Independent Advisor Alliance, a registered investment advisor. Independent Advisor Alliance and Thorium Wealth Management are separate entities from LPL Financial. The LPL Financial Registered Representatives associated with this site may only discuss and/or transact securities business with residents with the following states: NC, VA, GA, and FL.</p>
<br>
<strong>PHILIPPIANS 3:14</strong>
</div>
</div>
</footer>
<!-- END Footer Content -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="<?php  bloginfo('template_url');  ?>/assets/js/jquery.gmap.js"></script>
<script type="text/javascript">
jQuery(window).load( function(){
jQuery('#map').gMap({
address: '210 North Main Street, Suite 214 Kernersville, NC 27284',
maptype: 'ROADMAP',
zoom: 15,
markers: [
{
address: "210 North Main Street, Suite 214 Kernersville, NC 27284",
html: "<strong>Thorium Wealth Management</strong> <br> 210 North Main Street, Suite 214 <br> Kernersville, NC 27284 <br> <strong>(336) 310-4233</strong>",
icon: {
image: "http://sandbox.enlab.co/thoriumwealth/wp-content/themes/ThoriumWP/assets/images/icons/map-icon.png",
iconsize: [64, 64],
iconanchor: [14,44]
}
}
],
doubleclickzoom: false,
controls: {
panControl: false,
zoomControl: false,
mapTypeControl: false,
scaleControl: false,
streetViewControl: false,
overviewMapControl: false
},
styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]}]
});
});
</script>
<!-- END Footer -->

</div>
<!-- END #wrapper -->
<!-- Scroll To Top -->
<div id="scrollToTop" class="fa fa-chevron-up"></div>

<?php  wp_footer();  ?>

<script>
$(document).foundation();
</script>

<script>
  $('link').removeAttr('media');
</script>

</body>

</html>
