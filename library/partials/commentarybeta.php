<div id="work-section" class="section-page">
   <div class="row">
      <!-- Section Title -->
      <div class="section-title-block text-center centered-div" style="max-width: 800px;">
         <h2 class="title text-uppercase">Market Commentary</h2>
         <p class="lead">The market can be difficult to make heads or tails of. We hope to bring clarity with our commentary</p>
      </div>
   </div>
   <!-- Portfolio Top Margin -->
   <div class="portfolio-top-margin"></div>
   <!-- Portfolio Items -->
   <div id="portfolio" class="portfolio-nomargin portfolio-full portfolio-masonry mixed-masonry clearfix">
      <div class="portfolio-item f-design f-photography f-web wide">
         <div class="portfolio-image portfolio-expand-wrap">
            <a class="portfolio-expand-link" href="portfolio-expander.html">
            <img src="images/portfolio/mixed/1.jpg" alt="Sears">
            </a>
            <div class="portfolio-overlay">
               <div class="portfolio-desc">
                  <h3><a href="#">Sears</a></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="portfolio-item f-branding f-web f-video">
         <div class="portfolio-image portfolio-expand-wrap">
            <a class="portfolio-expand-link" href="portfolio-expander-video.html">
            <img src="images/portfolio/mixed/2.jpg" alt="Yielding">
            </a>
            <div class="portfolio-overlay">
               <div class="portfolio-desc">
                  <h3><a href="#">Yielding</a></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="portfolio-item f-design f-videography">
         <div class="portfolio-image">
            <img src="images/portfolio/mixed/3.jpg" alt="Bearing">
            <div class="portfolio-overlay">
               <div class="portfolio-desc">
                  <h3><a href="images/portfolio/full/p1.jpg" data-lightbox="image">Bearing</a></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="portfolio-item f-design wide">
         <div class="portfolio-image portfolio-expand-wrap">
            <a class="portfolio-expand-link" href="portfolio-expander.html">
            <img src="images/portfolio/mixed/4.jpg" alt="Culminating">
            </a>
            <div class="portfolio-overlay">
               <div class="portfolio-desc">
                  <h3><a href="#">Culminating</a></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="portfolio-item f-photography f-branding f-web wide">
         <div class="portfolio-image portfolio-expand-wrap">
            <a class="portfolio-expand-link" href="portfolio-expander.html">
            <img src="images/portfolio/mixed/5.jpg" alt="Seasons">
            </a>
            <div class="portfolio-overlay">
               <div class="portfolio-desc">
                  <h3><a href="#">Seasons</a></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="portfolio-item f-design f-photography f-branding f-web">
         <div class="portfolio-image">
            <img src="images/portfolio/mixed/6.jpg" alt="Open Day">
            <div class="portfolio-overlay">
               <div class="portfolio-desc">
                  <h3><a href="http://vimeo.com/113310561" data-lightbox="iframe">Open Day</a></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="portfolio-item f-design f-branding f-videography">
         <div class="portfolio-image portfolio-expand-wrap">
            <a class="portfolio-expand-link" href="portfolio-expander.html">
            <img src="images/portfolio/mixed/7.jpg" alt="Red Dominion">
            </a>
            <div class="portfolio-overlay">
               <div class="portfolio-desc">
                  <h3><a href="#">Red Dominion</a></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- #portfolio end -->
   <!-- Portfolio Top Margin -->
   <div class="portfolio-bottom-margin"></div>
   <!-- Portfolio Script -->
   <script type="text/javascript">
      jQuery(window).load(function(){
      
      var jQuerycontainer = jQuery('#portfolio');
      
      jQuerycontainer.isotope({
      transitionDuration: '0.65s',
      masonry: {
      columnWidth: jQuerycontainer.find('.portfolio-item:not(.wide)')[0]
      }
      });
      
      jQuery(window).resize(function() {
      jQuerycontainer.isotope('layout');
      WEPRO.portfolio.portfolioDescMargin();
      });
      
      var t = setTimeout(function(){ WEPRO.portfolio.portfolioDescMargin(); }, 200);
      
      
      });
   </script>
   <!-- Portfolio Script End -->
   <!--  Page Holder-->
   <div id="portfolio-page-holder" class="clearfix">
      <div id="portfolio-page-data"></div>
   </div>
</div>