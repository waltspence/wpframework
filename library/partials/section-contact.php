
<!-- Contact -->
<div id="contact-section" class="section-page overlay dark-bg" style="background-image: url(<?php  bloginfo('template_url');  ?>/assets/images/page/c1.jpg);">
<div id="contact-content">
<div class="section-title-block text-center">
<h2 class="title text-uppercase">Contact Us</h2>
<p class="lead">Send us an email. We'd love to hear from you!</p>
</div>
<div class="row">
<div class="medium-6 columns contact-section">
<header><meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<h4>Thank you</h4>
</header>
<div class="content">
<p>We appreciate your time and consideration. Feel free to contact us. We are here to serve you.</p>
</div>
<div class="contact-info">
<div class="header">
<p>Have questions? Drop us a line.</p>
</div>
<div class="method">
<div class="label"><i class="fa fa-phone"></i></div>
<div class="details"><a href="tel:+13363104233">(336) 310-4233</a></div>
</div>
<div class="method">
<div class="label"><i class="fa fa-envelope"></i></div>
<div class="details"><a href="mailto:Peter.Huminski@lpl.com?subject=Inquiry%20from%20ThroiumWealth.com">Peter.Huminski@lpl.com</a></div>
</div>
<div class="method">
<div class="label"><i class="fa fa-map"></i></div>
<div class="details">210 North Main Street,<br>
Suite 214<br>
Kernersville, NC 27284
</div>
</div>
</div>
</div>
<div class="medium-6 columns">
<div id="contact-form-result" data-notify-type="success" data-notify-msg="&lt;i class=fa fa-check&gt;&lt;/i&gt; Message Sent Successfully!"></div>
<form method="post" action="<?php  bloginfo('template_url');  ?>/library/inc/sendemail.php" id="wp-contact-form" name="wp-contact-form" class="contact-form">
<div class="form-processing"><i class="fa fa-spinner fa-pulse fa-3x"></i></div>
<label for="wp-contact-form-name">Full Name<span class="field_required">*</span></label>
<input name="wp-contact-form-name" id="wp-contact-form-name" class="required form-control" type="text" value="" tabindex="1">
<label for="wp-contact-form-email">Email<span class="field_required">*</span></label>
<input name="wp-contact-form-email" id="wp-contact-form-email" class="required form-control" type="email" value="" tabindex="2">
<label for="wp-contact-form-phone">Phone</label>
<input name="wp-contact-form-phone" id="wp-contact-form-phone" class="form-control" type="text" value="" tabindex="3">
<label for="wp-contact-form-subject">Subject</label>
<input name="wp-contact-form-subject" id="wp-contact-form-subject" class="form-control" type="text" value="" tabindex="4">
<label for="wp-contact-form-message">Message<span class="field_required">*</span></label>
<textarea id="wp-contact-form-message" name="wp-contact-form-message" class="required form-control" tabindex="5" rows="10" cols="50"></textarea>
<input type="submit" class="button" value="Submit" tabindex="5">
<input type="text" id="wp-contact-form-botcheck" name="wp-contact-form-botcheck" value="" class="form-control hidden">
</form>
<script type="text/javascript">
$("#wp-contact-form").validate({
submitHandler: function(form) {
$('.form-processing').fadeIn();

$(form).ajaxSubmit({
target: '#contact-form-result',
success: function() {
$('.form-processing').fadeOut();

$(form).find('.form-control').val('');
$('#contact-form-result').attr('data-notify-msg', $('#contact-form-result').html()).html('');
WEPRO.widget.notifications($('#contact-form-result'));
}
});
}
});

</script>
</div>
</div>
</div>
</div>

<!-- Google map -->
<div id="map" class="hide-for-small-only"></div>
<!-- END Contact -->
