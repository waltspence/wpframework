<?php
/*
Template Name: Single Post Template
*/
get_header(); 

// Inital Template Partials
get_template_part( 'library/partials/nav', 'global' ); 
get_template_part( 'library/partials/splash', 'small' ); 
?>

<div id="base">
	
<!-- Main Content -->
<div id="main-content" class="row">

<section id="single-post" role="main">
	<?php get_template_part( 'partials/featured-image' ); ?>

<?php do_action( 'cms_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class('content') ?> id="post-<?php the_ID(); ?>">
		<header>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php cms_entry_meta(); ?>
		</header>
		<?php do_action( 'cms_post_before_entry_content' ); ?>
		<div class="entry-content">

		<?php
			if ( has_post_thumbnail() ) :
				the_post_thumbnail();
			endif;
		?>

		<?php the_content(); ?>
		</div>
		<footer class="callout">
			<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'cms' ), 'after' => '</p></nav>' ) ); ?>
			<p><?php the_tags(); ?></p>
		</footer>

	</article>
<?php endwhile;?>

<?php do_action( 'cms_after_content' );  ?>

</section>
</div>
<!-- End Main Content -->

</div>
<!-- ./base -->

<?php get_footer(); ?>