<?php
/**
 * Custom Login Thang
 *
 * @package ThoriumWP
 * @since ThoriumWP 1.0.0
 */
 
function cms_custom_login() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . 'library/inc/login/custom-login-styles.css" />';
}
add_action('login_head', 'cms_custom_login');

function cms_login_logo_url() {
return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'cms_login_logo_url' );

function cms_login_logo_url_title() {
return '<?php echo get_bloginfo( 'name' ); ?>';
}
add_filter( 'login_headertitle', 'cms_login_logo_url_title' );