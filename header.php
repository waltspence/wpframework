<?php // add meta variables
$blog_title = get_bloginfo( 'name' ); 
$site_url = network_site_url( '/' );
$site_description = get_bloginfo( 'description' );
$current_url = home_url(add_query_arg(array(),$wp->request));
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php wp_head(); ?>
<meta name="description" content="<?php echo $site_description;?>" />

<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/icons/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/images/icons/apple-touch-icon-152x152.png" />

<!-- iPhone App -->
<!-- #soon <meta name="apple-itunes-app" content="app-id=853467066"> -->

<!-- Short url -->
<link rel="shortlink" href="http://thoriumne.ws">

<!-- twitter Site Info -->
<meta name="twitter:site" content="@ThoriumWealth">
<meta name="twitter:creator" content="@ThoriumWealth">
<meta name="twitter:card" content="summary_large_image">

<!-- twitter Page Info -->
<meta name="twitter:title" content="<?php echo $blog_title; ?>">
<meta name="twitter:description" content="<?php echo $site_description; ?>">
<meta name="twitter:image" content="<?php bloginfo('template_url'); ?>/images/icons/thoriumwealth.png">

<!-- Fb Open Graph Site Info -->
<meta property="og:site_name" content="<?php echo $blog_title; ?>"/>
<meta property="og:type" content="website" />

<!-- Fb Open Graph Page Info -->
<meta property="og:title" content="<?php echo $blog_title; ?> | <?php echo $post_title; ?>" />
<meta property="og:description" content="<?php echo $site_description; ?>" />
<meta property="og:url" content="<?php echo $current_url; ?>" />

<!---------------------------------------------------------------->
<meta property="og:image" content="<?php bloginfo('template_url'); ?>/images/thorium_og.png" />
<!-- s//o <meta property="og:video" content="" /> o//n -->

<!-- humans and robots -->
<link rel="author" href="/humans.txt">
<meta name="robots" content="index, follow">


</head>

<body data-loader="3">
<!-- Document Wrapper -->
<div id="wrapper" class="clearfix">
