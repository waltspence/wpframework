<style type="text/css">@import url(http:///tw.enlab.co/assets/css/flow.css);</style>

	<section id="cd-timeline" class="cd-container">
		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-picture">
				<img src="http://thoriumwealth.com/assets/images/svg-src/cd-icon-picture.svg" alt="Picture">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<h2>STEP 1: IS IT A FIT?</h2>
			<ul>
			 	<li>What I believe differentiates me from others in the industry</li>
			 	<li>My Process</li>
			</ul>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->

		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-movie">
				<img src="http://thoriumwealth.com/assets/images/svg-src/cd-icon-picture.svg" alt="Movie">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<h2>STEP 2: INFORMATION GATHERING MEETING</h2>
				<ul>
				 	<li>Financial Planning Process and Goals</li>
				 	<li>Gather all documents and statements for proposal</li>
				</ul>

			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->

		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-picture">
				<img src="http://thoriumwealth.com/assets/images/svg-src/cd-icon-picture.svg" alt="Picture">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<h2>STEP 3: PRESENTATION MEETING</h2>

				<ul>
				 	<li>Present initial planning findings</li>
				 	<li>Present investment proposal</li>
				 	<li>Discuss costs</li>
				 	<li>Design service schedule with you</li>
				 	<li>Sign account paperwork</li>
				</ul>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->

		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-location">
				<img src="http://thoriumwealth.com/assets/images/svg-src/cd-icon-picture.svg" alt="Location">
			</div> <!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<h2>STEP 4: ONGOING REVIEW MEETINGS</h2>

				<ul>
				 	<li>Performance and investment review</li>
				 	<li>Ongoing financial education topics</li>
				 	<li>Ongoing financial plan review</li>
				</ul>
			</div> <!-- cd-timeline-content -->
		</div> <!-- cd-timeline-block -->

		<div class="cd-timeline-block">
			<div class="cd-timeline-img cd-location">
				<img src="http://thoriumwealth.com/assets/images/svg-src/cd-icon-picture.svg" alt="Location">
			</div> <!-- cd-timeline-img -->

	</section> <!-- cd-timeline -->