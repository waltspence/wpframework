<?php
/*
Template Name: 404
*/
get_header(); 

// Inital Template Partials
get_template_part( 'library/partials/nav', 'global' ); 
get_template_part( 'library/partials/splash', 'small' ); 
?>

<div id="base">

<!-- Main Content -->
<div id="main-content" class="row">

<div class="row">
	<div class="small-12 large-8 columns" role="main">

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php _e( 'File Not Found', 'cms' ); ?></h1>
			</header>
			<div class="entry-content">
				<div class="error">
					<p class="bottom"><?php _e( 'The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'cms' ); ?></p>
				</div>
				<p><?php _e( 'Please try the following:', 'cms' ); ?></p>
				<ul>
					<li><?php _e( 'Check your spelling', 'cms' ); ?></li>
					<li><?php printf( __( 'Return to the <a href="%s">home page</a>', 'cms' ), home_url() ); ?></li>
					<li><?php _e( 'Click the <a href="javascript:history.back()">Back</a> button', 'cms' ); ?></li>
				</ul>
			</div>
		</article>

	</div>
	<?php get_sidebar(); ?>
</div>

</div>
<!-- End Main Content -->

</div>
<!-- ./base -->

<?php get_footer();?>