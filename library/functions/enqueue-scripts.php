<?php
/**
* Enqueue all styles and scripts
*
* Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
* Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
*
*@package ThoriumWP
*@since ThoriumWP 1.0.0
*/

if ( ! function_exists( 'cms_scripts' ) ) :
function cms_scripts() {

//BEGIN CSS

// Enqueue the Core Stylesheets
wp_enqueue_style( 'foundation', get_template_directory_uri() . '/assets/css/foundation.css', array(), null, false );
wp_enqueue_style( 'app-core', get_template_directory_uri() . '/assets/css/app.css', array(), null, false );
wp_enqueue_style( 'core', get_template_directory_uri() . '/style.css', array(), null, false );

// Vendor Stylesheets
wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), null, false );
wp_enqueue_style( 'linear-icons', get_template_directory_uri() . '/assets/css/linear-icons.css', array(), null, false );
wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css', array(), null, false );
wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/assets/css/magnific-popup.css', array(), null, false );
wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Raleway:600,700|Open+Sans:400,300,600,700', array(), null, false );

//END CSS

//BEGIN JS

// Deregister the jquery version bundled with WordPress.
wp_deregister_script( 'jquery' );

// Header JS
wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.js', array(), null, false );
wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr.min.js', array(), null, false );
wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/assets/js/jquery-ui.min.js', array(), null, false );
wp_enqueue_script( 'plugins', get_template_directory_uri() . '/assets/js/plugins.js', array(), null, false );
wp_enqueue_script( 'scrollReveal', get_template_directory_uri() . '/assets/js/scrollReveal.min.js', array(), null, false );

// Footer JS
wp_enqueue_script( 'what-input', get_template_directory_uri() . '/assets/js/vendor/what-input.min.js', array(), null, true );
wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/js/foundation.min.js', array(), null, true );
wp_enqueue_script( 'classie', get_template_directory_uri() . '/assets/js/classie.js', array(), null, true );
wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/assets/js/fitvids.js', array(), null, true );
wp_enqueue_script( 'app.js mobile', get_template_directory_uri() . '/assets/js/app.js', array(), null, true );

//END JS

// Add the comment-reply library on pages where it is necessary
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
wp_enqueue_script( 'comment-reply' );
}

}

add_action( 'wp_enqueue_scripts', 'cms_scripts' );
endif;
