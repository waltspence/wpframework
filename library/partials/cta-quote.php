
<!--CTA Quote -->
<div class="section-page dark-bg" style="background-image: url(<?php  bloginfo('template_url');  ?>/assets/images/page/quote_bg.jpg);" data-stellar-background-ratio="0.3">
<div class="row text-center">
<div class="quote">
<blockquote>
<p>By failing to prepare, <span class="strong">you are preparing to fail</span>.</p>
<span class="author">– Benjamin Franklin</span>
</blockquote>
</div>
</div>
</div>
<!-- END CTA Quote -->
