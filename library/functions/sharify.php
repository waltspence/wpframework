<?php
/**
 * Sharify (Social Buttons for Single Posts)
 *
 *@package ThoriumWP
 *@since ThoriumWP 1.0.0
 */

 function sharify_social_sharing_buttons($content) {
		if ( is_single() ) {

		// Get Site URL
		$sharifySource = get_site_url();

		// Get current page URL 
		$sharifyURL = get_permalink();

		// Get current page title
		$sharifyTitle = str_replace( ' ', '%20', get_the_title());


		// Get Post Thumbnail for pinterest
		$sharifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$sharifyTitle.'&amp;url='.$sharifyURL.'&amp;via=ThoriumWealth.com';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$sharifyURL;
		$linkedinURL = 'http://www.linkedin.com/shareArticle?mini=true&url='.$sharifyURL.'&amp;title='.$sharifyTitle.'&amp;summary='.$sharifyURL.'&amp;source='.$sharifySource;
		$googleURL = 'https://plus.google.com/share?url='.$sharifyURL;
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$sharifyURL.'&amp;media='.$sharifyThumbnail[0].'&amp;description='.$sharifyTitle;

		// Add sharing button at the end of page/page content
		$content .= '<div class="share">';
		$content .= '<a data-reveal-id="share_modal" data-reveal-ajax="true" href="'. $facebookURL .'" class="button medium facebook social"> <i class="fa fa-facebook"></i></a>';
		$content .= '<a data-reveal-id="share_modal" data-reveal-ajax="true" href="'. $twitterURL .'" class="button medium twitter social"> <i class="fa fa-twitter"></i></a>';
		$content .= '<a data-reveal-id="share_modal" data-reveal-ajax="true" href="'. $linkedinURL .'" class="button medium linkedin social"> <i class="fa fa-linkedin"></i></a>';
		$content .= '</div>';

		return $content;
 	}else{
 		// if not a post/page then don't include sharing button
 		return $content;
 	}
 };
 add_filter( 'the_content', 'sharify_social_sharing_buttons');