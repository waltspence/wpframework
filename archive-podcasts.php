<?php
/*
Template Name: Finance Factory Index
*/

//Get WP Header
get_header(); 

// Inital Template Partials

get_template_part( 'library/partials/nav', 'global' ); 
get_template_part( 'library/partials/splash', 'small' ); 

?>

<div class="callout large primary">
<div class="row column text-center">
<h1>The Finance Factory Podcasts</h1>
</div>
</div>
<div id="base">

<!-- Main Content -->
<div id="main-content" class="row">
  <div class="medium-8 columns">
  
  <?php if ( have_posts() ) : ?>

    <?php /* Start the Loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'library/partials/content', get_post_format() ); ?>
    <?php endwhile; ?>

    <?php else : ?>
      <?php get_template_part( 'library/partials/content', 'none' ); ?>

    <?php endif; // End have_posts() check. ?>

    <?php /* Display navigation to next/previous pages when applicable */ ?>
    <?php if ( function_exists( 'cms_pagination' ) ) { cms_pagination(); } else if ( is_paged() ) { ?>
      <nav id="post-nav">
        <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'cms' ) ); ?></div>
        <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'cms' ) ); ?></div>
      </nav>
    <?php } ?>
    
  </div>
  
  <?php get_sidebar(); ?>

</div>
<!-- After Content -->        
<?php do_action( 'cms_after_content' ); ?>
<!-- End Main Content -->

</div>
<!-- ./base -->
<?php get_footer(); ?>