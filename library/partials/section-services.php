
<!-- Services -->
<div id="services-section" class="section-page">
<div class="row">
<div class="section-title-block text-center centered-div" style="max-width: 800px;">
<h2 class="title text-uppercase">Our Services</h2>
<p class="lead">Our personal approach to client service is key to the service we provide.</p>
</div>
</div>
<div class="row">
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.1s, scale up 50%">
<div class="icon-block-icon icon-large">
<a href="services/wealthmanagement"><i class="fa fa-money"></i></a>
</div>
<a href="services/wealthmanagement"><h3>Wealth Management</h3></a>
<p>For better or for worse, life happens in transitions.</p>
</div>
</div>
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.2s, scale up 45%">
<div class="icon-block-icon icon-large">
<a href="services/financialplanning"><i class="fa fa-bank"></i></a>
</div>
<a href="services/financialplanning"><h3>Financial Planning</h3></a>
<p>The path toward your financial future requires a clear vision, a detailed plan, and a dynamic team.</p>
</div>
</div>
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.3s, scale up 40%">
<div class="icon-block-icon icon-large">
<a href="services/investmentmangement"><i class="fa fa-line-chart"></i></a>
</div>
<a href="services/investmentmanagement"><h3>Investment Management</h3></a>
<p>Gaining insight into a client's life and financial situation is at the core of our wealth management strategy.</p>
</div>
</div>
</div>
<div class="row">
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.4s, scale up 35%">
<div class="icon-block-icon icon-large">
<a href="services/retirementplanning"><i class="fa fa-road"></i></a>
</div>
<a href="services/retirementplanning"><h3>Retirement Planning</h3></a>
<p>Retirement is something that you create with the decisions you make every day.</p>
</div>
</div>
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.5s, scale up 30%">
<div class="icon-block-icon icon-large">
<a href="services/riskmanagement"><i class="lnr lnr-warning"></i></a>
</div>
<a href="services/riskmanagement"><h3>Risk Management</h3>
<p>Reducing risk is at the core of our philosophy and process.</p>
</div>
</div>
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.6s, scale up 25%">
<div class="icon-block-icon icon-large">
<a href="services/businessplanning"><i class="fa fa-briefcase"></i></a>
</div>
<a href="services/businessplanning"><h3>Business Planning</h3></a>
<p>Thorium’s founder has a long history of helping entrepreneurs determine the proper strategies for their businesses.</p>
</div>
</div>
</div>
<div class="row">
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.7s, scale up 20%">
<div class="icon-block-icon icon-large">
<a href="services/estateplanning"><i class="lnr lnr-home"></i></a>
</div>
<a href="services/estateplanning"><h3>Estate Planning</h3></a>
<p>Your work in building wealth should reward your family, not the government.</p>
</div>
</div>
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.8s, scale up 15%">
<div class="icon-block-icon icon-large">
<a href="services/charitableplanning"><i class="lnr lnr-heart"></i></a>
</div>
<a href="services/charitableplanning"><h3>Charitable Planning</h3></a>
<p>This is true no matter how grand or modest a contribution you can afford.</p>
</div>
</div>
<div class="medium-4 column">
<div class="icon-block text-center" data-sr="wait 0.9s, scale up 10%">
<div class="icon-block-icon icon-large">
<a href="services/educationplanning"><i class="fa fa-graduation-cap"></i></a>
</div>
<a href="services/educationplanning"><h3>Education Planning</h3>
<p>College costs money and lots of it.</p>
</div>
</div>
</div>
</div>
<!--END Services -->
